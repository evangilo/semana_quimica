# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('minicursos', '0006_auto_20150607_0220'),
    ]

    operations = [
        migrations.AddField(
            model_name='minicurso',
            name='num_inscritos',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='minicursousuario',
            name='minicurso',
            field=models.ForeignKey(related_name='usuarios', to='minicursos.Minicurso'),
        ),
        migrations.AlterField(
            model_name='minicursousuario',
            name='usuario',
            field=models.ForeignKey(related_name='minicursos', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterUniqueTogether(
            name='minicursousuario',
            unique_together=set([('usuario', 'minicurso')]),
        ),
    ]
