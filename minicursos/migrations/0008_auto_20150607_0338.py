# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('minicursos', '0007_auto_20150607_0338'),
    ]

    operations = [
        migrations.AlterField(
            model_name='minicurso',
            name='vagas',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
