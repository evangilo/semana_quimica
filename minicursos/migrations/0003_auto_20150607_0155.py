# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('minicursos', '0002_auto_20150607_0153'),
    ]

    operations = [
        migrations.AlterField(
            model_name='palestrante',
            name='bio',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='palestrante',
            name='thumbnail',
            field=models.ImageField(null=True, upload_to=b'photos/speakers', blank=True),
        ),
    ]
