# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('minicursos', '0004_auto_20150607_0203'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='dataehora',
            options={'ordering': ['data', 'hora_inicial']},
        ),
        migrations.AlterField(
            model_name='dataehora',
            name='minicurso',
            field=models.ForeignKey(related_name='data_e_hora', to='minicursos.Minicurso'),
        ),
    ]
