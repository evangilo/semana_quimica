# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('minicursos', '0005_auto_20150607_0210'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='minicurso',
            options={'ordering': ['data_e_hora', 'titulo']},
        ),
        migrations.AlterField(
            model_name='minicursousuario',
            name='minicurso',
            field=models.ForeignKey(related_name='usuarios_inscritos', to='minicursos.Minicurso'),
        ),
        migrations.AlterField(
            model_name='minicursousuario',
            name='usuario',
            field=models.ForeignKey(related_name='minicurso_usuario', to=settings.AUTH_USER_MODEL),
        ),
    ]
