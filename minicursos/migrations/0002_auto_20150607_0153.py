# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('minicursos', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='palestrante',
            name='bio',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='palestrante',
            name='thumbnail',
            field=models.ImageField(upload_to=b'photos/speakers', blank=True),
        ),
    ]
