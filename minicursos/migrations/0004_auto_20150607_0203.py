# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('minicursos', '0003_auto_20150607_0155'),
    ]

    operations = [
        migrations.AlterField(
            model_name='minicurso',
            name='descricao',
            field=models.TextField(null=True, verbose_name=b'descri\xc3\xa7\xc3\xa3o', blank=True),
        ),
        migrations.AlterField(
            model_name='minicurso',
            name='pre_requisitos',
            field=models.TextField(null=True, verbose_name=b'pr\xc3\xa9 requisitos', blank=True),
        ),
    ]
