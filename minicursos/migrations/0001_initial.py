# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='DataEHora',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data', models.DateField()),
                ('hora_inicial', models.TimeField()),
                ('hora_final', models.TimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Minicurso',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=100, verbose_name=b't\xc3\xadtulo')),
                ('descricao', models.TextField(verbose_name=b'descri\xc3\xa7\xc3\xa3o')),
                ('pre_requisitos', models.TextField(verbose_name=b'pr\xc3\xa9 requisitos')),
                ('vagas', models.PositiveIntegerField()),
            ],
            options={
                'ordering': ['titulo'],
            },
        ),
        migrations.CreateModel(
            name='MinicursoUsuario',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('minicurso', models.ForeignKey(to='minicursos.Minicurso')),
                ('usuario', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Palestrante',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=100)),
                ('thumbnail', models.ImageField(upload_to=b'photos/speakers')),
                ('bio', models.TextField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Sala',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('local', models.CharField(max_length=20)),
            ],
        ),
        migrations.AddField(
            model_name='minicurso',
            name='palestrante',
            field=models.ForeignKey(to='minicursos.Palestrante'),
        ),
        migrations.AddField(
            model_name='minicurso',
            name='sala',
            field=models.ForeignKey(to='minicursos.Sala'),
        ),
        migrations.AddField(
            model_name='dataehora',
            name='minicurso',
            field=models.ForeignKey(to='minicursos.Minicurso'),
        ),
    ]
