# -*- coding: utf-8 -*-
from itertools import groupby

from PIL import Image
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
import os

from base.models import CustomUser


class Palestrante(models.Model):
    nome = models.CharField(max_length=100)
    thumbnail = models.ImageField(upload_to="photos/speakers", blank=True, null=True)
    bio = models.TextField(blank=True, null=True)

    def get_thumbnail_filename(self):
        return os.path.join('media', self.thumbnail.name)

    def save(self, size=(300, 300)):
        super(Palestrante, self).save()
        if self.thumbnail:
            filename = self.get_thumbnail_filename()
            image = Image.open(filename)
            image.thumbnail(size, Image.ANTIALIAS)
            image.save(filename)

    def __unicode__(self):
        return self.nome


class Sala(models.Model):
    local = models.CharField(max_length=20)

    def __unicode__(self):
        return self.local


class Minicurso(models.Model):
    palestrante = models.ForeignKey(Palestrante)
    sala = models.ForeignKey(Sala)
    titulo = models.CharField(u"título", max_length=100)
    descricao = models.TextField(u"descrição", null=True, blank=True)
    pre_requisitos = models.TextField(u"pré requisitos", null=True, blank=True)
    vagas = models.PositiveIntegerField(default=0)
    num_inscritos = models.PositiveIntegerField(u"número de inscritos", default=0,
                                                validators=[MinValueValidator(0), MaxValueValidator(vagas)])

    def remover_inscricao(self):
        inscricao = self.num_inscritos - 1
        if inscricao >= 0:
            self.num_inscritos = inscricao
        else:
            raise u"Número de inscritos não pode ser menor que zero."

    @property
    def vagas_disponiveis(self):
        return self.vagas - self.num_inscritos

    @property
    def tem_vagas_disponiveis(self):
        return self.num_inscritos < self.vagas

    @property
    def datas(self):
        return self.data_e_hora.all()

    def __unicode__(self):
        return self.titulo

    class Meta:
        ordering = ['titulo']


class DataEHora(models.Model):
    minicurso = models.ForeignKey(Minicurso, related_name='data_e_hora')
    data = models.DateField()
    hora_inicial = models.TimeField()
    hora_final = models.TimeField()

    class Meta:
        ordering = ['data', 'hora_inicial']


class MinicursoUsuario(models.Model):
    usuario = models.ForeignKey(CustomUser, related_name='minicursos')
    minicurso = models.ForeignKey(Minicurso, related_name='usuarios')

    class Meta:
        unique_together = ['usuario', 'minicurso']
