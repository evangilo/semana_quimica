# -*- coding: utf-8 -*-
from django.contrib import admin
from minicursos.models import Palestrante, Sala, Minicurso, DataEHora, MinicursoUsuario


class DataEHoraInline(admin.TabularInline):
    model = DataEHora
    extra = 1

@admin.register(Minicurso)
class MinicursoAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'palestrante', 'sala', 'vagas')
    inlines = [
        DataEHoraInline,
    ]

@admin.register(Palestrante)
class PalestranteAdmin(admin.ModelAdmin):
    list_display = ('nome', 'bio')

@admin.register(Sala)
class SalaAdmin(admin.ModelAdmin):
    list_display = ('local',)

@admin.register(MinicursoUsuario)
class MinicursoUsuarioAdmin(admin.ModelAdmin):
    list_display = ('usuario', 'minicurso')
