# -*- coding: utf-8 -*-
from collections import OrderedDict

from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
from django.db.utils import IntegrityError
from django.shortcuts import render, redirect
from django.contrib import messages

from minicursos.models import Minicurso, MinicursoUsuario


@login_required
def detail_minicurso(request, pk):
    minicurso = Minicurso.objects.filter(pk=pk).first()
    return render(request, "detail_minicurso.html", {'minicurso': minicurso})


@login_required
def cancelar_inscricao(request, pk):
    minicurso_usuario = MinicursoUsuario.objects.get(usuario=request.user, minicurso=pk)
    if minicurso_usuario:
        minicurso = minicurso_usuario.minicurso
        minicurso.num_inscritos -= 1
        minicurso.save()
        minicurso_usuario.delete()
        messages.add_message(request, messages.SUCCESS, u"Inscrição cancelada.")
    else:
        messages.add_message(request, messages.INFO, u"Você não está inscrito neste minicurso.")
    return redirect(reverse_lazy("minicursos"))


@login_required
def inscriver_usuario_no_minicurso(request, pk):
    try:
        minicurso = Minicurso.objects.filter(pk=pk, num_inscritos__lt=Minicurso.objects.values('vagas')).first()
        inscritos = minicurso.num_inscritos + 1
        if minicurso and inscritos <= minicurso.vagas:
            MinicursoUsuario(usuario=request.user, minicurso=minicurso).save()
            minicurso.num_inscritos = inscritos
            minicurso.save()
            messages.add_message(request, messages.SUCCESS, u"Inscrição realizada com sucesso.")
        else:
            messages.add_message(request, messages.WARNING, u"Inscrições esgotadas.")
    except IntegrityError:
        messages.add_message(request, messages.ERROR,
                             u"%s você já está inscrito neste minicurso." % request.user.first_name)
    return redirect(reverse_lazy("minicursos"))


@login_required
def listar_minicursos(request):
    inscritos = OrderedDict.fromkeys(
        Minicurso.objects.filter(usuarios__usuario=request.user).order_by('data_e_hora', 'titulo'))
    nao_inscritos = OrderedDict.fromkeys(
        Minicurso.objects.exclude(usuarios__usuario=request.user).order_by('data_e_hora', 'titulo'))
    return render(request, 'list.html', {'inscritos': inscritos, 'nao_inscritos': nao_inscritos})
