# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse_lazy
from django.views.generic.edit import CreateView

from base.form import CustomRegistrationForm
from base.models import CustomUser


class UserCreateView(CreateView):
    model = CustomUser
    form_class = CustomRegistrationForm
    success_url = reverse_lazy("entrar")
    template_name = "registration/form.html"
