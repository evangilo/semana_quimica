# -*- coding: utf-8 -*-
from django.contrib import admin

from base.models import CustomUser

@admin.register(CustomUser)
class CustomUserAdmin(admin.ModelAdmin):
    list_display = ('nome_completo', 'cpf', 'email', 'instituicao', 'curso',)
