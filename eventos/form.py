# -*- coding: utf-8 -*-
from django import forms

from eventos.models import Trabalho


class TrabalhoForm(forms.ModelForm):
    def __init__(self, usuario_atual, *args, **kwargs):
        super(TrabalhoForm, self).__init__(*args, **kwargs)
        self.fields['inscricao'].queryset = self.fields['inscricao'].queryset.filter(usuario=usuario_atual)
        self.fields['inscricao'].empty_label = None
        self.fields['tipo'].empty_label = None

    class Meta:
        model = Trabalho
        fields = ('inscricao', 'tipo', 'titulo', 'autores', 'orientadores', 'artigo',)
        labels = {
            'inscricao': u'Inscrição',
            'tipo': u'Área',
        }
