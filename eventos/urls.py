# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^incricao_evento/(?P<pk>\d+)/$', 'eventos.views.inscrever_evento', name="inscricao_evento"),
    url(r'^minha_area/$', 'eventos.views.area_usuario', name="minha_area"),
    url(r'^submeter_trabalho/$', 'eventos.views.submeter_trabalho', name='submeter_trabalho'),
    url(r'^$', 'eventos.views.index', name='index'),
)
