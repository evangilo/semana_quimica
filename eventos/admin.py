# -*- coding: utf-8 -*-
from django.contrib import admin
from django.db import models
from django.forms import CheckboxSelectMultiple
from eventos.models import Evento, Trabalho, Inscricao, TipoTrabalho


@admin.register(Evento)
class InscricaoAdmin(admin.ModelAdmin):
    list_display = ('descricao', 'data_inicial', 'data_final')
    formfield_overrides = {
        models.ManyToManyField: {'widget': CheckboxSelectMultiple}
    }

@admin.register(TipoTrabalho)
class TipoTrabalhoAdmin(admin.ModelAdmin):
    list_display = ('tipo',)

@admin.register(Trabalho)
class TrabalhoAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'tipo', 'nome_do_autor', 'email_do_autor', 'status',)


@admin.register(Inscricao)
class InscricaoAdmin(admin.ModelAdmin):
    list_display = ('usuario', 'evento',)
