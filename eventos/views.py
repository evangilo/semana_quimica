# -*- coding: utf-8 -*-
from datetime import date

from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse, reverse_lazy
from django.shortcuts import redirect, render_to_response, render
from django.contrib import messages

from eventos.form import TrabalhoForm
from eventos.models import Inscricao, Evento, Trabalho



def index(request):
    return render(request, 'home.html')


@login_required
def inscrever_evento(request, pk):
    evento = Evento.objects.get(pk=pk)
    Inscricao(evento=evento, usuario=request.user).save()
    messages.add_message(request, messages.SUCCESS, "Inscrição realizada com sucesso.")
    return redirect(reverse_lazy('minha_area'))


@login_required
def area_usuario(request):
    novos_eventos = Evento.objects.exclude(inscricoes__usuario=request.user).filter(data_final__gte=date.today())
    eventos_inscritos = Evento.objects.filter(inscricoes__usuario=request.user, data_final__gte=date.today())
    trabalhos = Trabalho.objects.all().filter(inscricao__usuario=request.user)
    return render(request, 'area_usuario.html', {
        'novos_eventos': novos_eventos,
        'eventos_inscritos': eventos_inscritos,
        'trabalhos': trabalhos})


@login_required
def submeter_trabalho(request):
    if request.method == 'POST':
        form = TrabalhoForm(request.user, request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, "Trabalho enviado com sucesso.")
            return redirect(reverse_lazy('minha_area'))
    else:
        form = TrabalhoForm(request.user)
    return render(request, 'submissao_artigo.html', {'form': form})
